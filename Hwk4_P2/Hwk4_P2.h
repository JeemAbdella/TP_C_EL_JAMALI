#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// get randoom int between two int values
int getRandomNumber(){
    int low = 1, height = 1000;
    return (rand() % height ) + low ;
}

// Game engine
void play(int winningNumber){
    // Init variables 
    int guess ;
    char answer ;
    bool replay = true, isValidAnswer = false;

    // check if it's a new game or a retry
    if(winningNumber < 1){
        winningNumber = getRandomNumber();
    }

    // ask for a guess 
    printf("Guess The Number :\n");
    // retrieve the guess
    scanf("%d",&guess);

    // if the guess is correct 
    if (winningNumber == guess){
        printf("Excellent ! you guessed the number !");

        do{
            // Ask to replay
            printf("Would you like to play again (y or n) ?\n");
            scanf(" %c",&answer);

            // answer is No 
            if(answer == 'N' || answer == 'n') {
                isValidAnswer = true;
                replay = false;
            }
            //answer is Yes
            else if(answer == 'Y' || answer == 'y'){
                winningNumber = 0;
                isValidAnswer = true;
            }
        }
        //repeat till the answer is (y or n)
        while( isValidAnswer == false );

    }
    // if guess is lower 
    else if(winningNumber > guess){
        printf("Too low. Try again");
    }
    // if guess is greater 
    else if(winningNumber < guess){
        printf("Too high. Try again");
    }

    // replay if user wants to reply or should retry
    if(replay){
            play(winningNumber);
    }
}

