
// Function that estimates PI
double estimate_PI(int terms){

    // Handle 0 case
    if( terms < 1 ) return 0;

    // Init variables
    int dividend = 4, divisor  = 1, signature = 1, i = 0;
    double sum = 0;

    // Increment to next divisor
    while(i<terms){
        // If odd number
        if(divisor%2 != 0){
            sum += (signature * ((double)dividend/(double)divisor));
            // Switch signature
            signature *= -1;
            i++;
        }
        divisor++;
    }
    return sum;
}

