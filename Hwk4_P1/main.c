#include <stdio.h>
#include <stdlib.h>
#include "Hwk4_P1.h"

int main()
{
    int terms ;
    // Read user input
    printf("How many terms would you like to calculate PI to : ");
    scanf("%d",&terms);

    // Calculate estimation
    double estimation = estimate_PI(terms);

    // Print estimation
    printf("%.6lf",estimation);
    return 0;
}
